package withouther.system;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import withouther.system.plugins.system.service.RoleService;
import withouther.system.plugins.system.service.UserService;

import javax.annotation.Resource;

@SpringBootTest
class SystemApplicationTests {

    @Resource
    private UserService userService;
    @Resource
    private RoleService roleService;

    @Test
    void contextLoads() {
//        System.out.println(LocalDateTime.now());
//        roleService.selectUserRoleListByUserId(2l);
//        System.out.println(LocalDateTime.now());
        System.out.println( roleService.selectUserRoleListByUserId(2l));
        System.out.println( roleService.selectUserRoleListByUserId(2l));
//        List<Role> role = roleService.getBaseMapper().selectByMap(null);
//        System.out.println(role);
//        Map<String, Object> map = new HashMap<>();
//        map.put("userName", "admin");
//        map.put("page", "0");
//        map.put("limit", "10");
//        System.out.println(userService.queryPage(map));
    }

    /**
     * 缓存数据
     * 运行流程
     * @Cachedle:
     * 1.方法运行之前，先去查询Cache(缓存组件)，按照cacheNames指定的名字获取；
     *  （CacheManager先获取相应的缓存），第一个获取缓存如果没有cache组件会自动创建。
     *  2.去Cache中查询缓存内容，使用一个key，默认就是方法的参数；
     *      key是按照keyGeneratror 生成Key的，默认使用SimplekeyGeneratror 生成的
     *  3.没有查到缓存就调用目标方法、
     *  4.将目标返回的结果，放进缓存中
     *  核心：
     *  1.使用CacheManager作为缓存中心
     * 自动配置类：
     *
     */
    @Cacheable
    public  void cacheable() {

    }

    /**
     * 更新缓存中的数据
     *  @CachePut,即调用方法，也更新缓存
     *  运行时机
     *  1：先调用方法，在更新缓存
     *  2.将目标缓存
     *  3.更新1号员工
     *      将方法的返回值也放进缓存了，
     *      key:对象， value:返回的对象
     */
    @CachePut
    public  void cachePut() {

    }

    /**
     * 缓存清除
     * key = 要清除的key
     * allEntries = true:指定清除这个缓存中所有的数据
     * beforeInvocation = false : 缓存的清除是否在方法之前执行
     *          默认代表是方法执行之后执行
     *          = true ：方法之前删除，这样可以不考虑异常
     */
    @CacheEvict(value="emp")
    public void cacheEvict() {

    }


}
