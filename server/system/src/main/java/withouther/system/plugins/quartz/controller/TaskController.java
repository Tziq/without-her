package withouther.system.plugins.quartz.controller;

import cn.hutool.core.util.ClassUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.List;

import withouther.system.plugins.quartz.config.JobQuartzManager;
import withouther.system.plugins.quartz.config.job.JobTestTask;
import withouther.system.plugins.quartz.entity.Task;
import withouther.system.plugins.quartz.service.TaskService;
import withouther.system.core.base.BaseController;
import withouther.system.core.result.WhResult;


/**
 * @author tzq
 * @date 2021-02-19
 */
@Api(tags = "/api/管理")
@RestController
@RequestMapping("/api/task")
@Slf4j
public class TaskController extends BaseController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private JobQuartzManager jobQuartzManager;

    @ApiOperation("列表")
    @GetMapping("/list")
    public WhResult page(@RequestParam Map<String, Object> params) {
        try {
            return WhResult.success(taskService.queryPage(params));
        } catch (Exception e) {
            log.error("page error", e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @ApiOperation("获取详情")
    @GetMapping("/get/{id}")
    public WhResult get(@PathVariable("id") Long id) {
        try {
            return WhResult.success(taskService.getById(id));
        } catch (Exception e) {
            log.error("get error", e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @ApiOperation("添加")
    @PostMapping("/add")
    public WhResult add(@RequestBody Task task) {
        try {
//            boolean success = jobQuartzManager.addJob("job" + task.getId(), dataBaseId, JobTestTask.class, cronExp);
//            if (!success) {
//                return WhResult.fail("创建失败");
//            }
            Class beanClass=  Class.forName(task.getBeanClass());
            return WhResult.success(taskService.save(task));
        } catch (ClassNotFoundException e){
            log.error("The address of the corresponding class was not found", e);
            return WhResult.fail("类地址不准确，请使用这种格式(xx.xx.xx)");
        } catch (Exception e) {
            log.error("add error", e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @ApiOperation("修改")
    @PutMapping("/update")
    public WhResult modify(@RequestBody Task task) {
        try {
            return WhResult.success(taskService.updateById(task));
        } catch (Exception e) {
            log.error("modify error", e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @ApiOperation("启动暂停Job")
    @PutMapping("/startOrStopJob")
    public WhResult startOrStopJob(@RequestBody Task task) {
        try {
            if ("1".equals(task.getJobStatus())) {
                Class beanClass=  Class.forName(task.getBeanClass());
                boolean success = jobQuartzManager.addJob("job" + task.getId(), task.getId(), beanClass, task.getCronExpression());
                if (!success) {
                    return WhResult.fail("启动失败");
                }
            } else {
                boolean success = jobQuartzManager.deleteJob("job" + task.getId());
                if (!success) {
                    return WhResult.fail("暂停失败");
                }
            }
            return WhResult.success(taskService.updateById(task));
        } catch (Exception e) {
            log.error("modify error", e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @ApiOperation("删除")
    @DeleteMapping("/delete")
    public WhResult delete(@RequestBody List<Long> ids) {
        try {
            return WhResult.success(taskService.removeByIds(ids));
        } catch (Exception e) {
            log.error("delete error", e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }
}