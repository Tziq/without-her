package withouther.system.plugins.quartz.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.*;

/**
 * @author tzq
 * @date 2021-02-19
 */
@Data
@TableName("sys_task")
@ApiModel(value = "Task对象")
public class Task implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    @TableField("id")
    private Long id;

    @ApiModelProperty(value = "任务名")
    @TableField("job_name")
    private String jobName;

    @ApiModelProperty(value = "任务描述")
    @TableField("description")
    private String description;

    @ApiModelProperty(value = "cron表达式")
    @TableField("cron_expression")
    private String cronExpression;

    @ApiModelProperty(value = "任务执行时调用哪个类的方法 包名+类名")
    @TableField("bean_class")
    private String beanClass;

    @ApiModelProperty(value = "任务状态")
    @TableField("job_status")
    private String jobStatus;

    @ApiModelProperty(value = "任务分组")
    @TableField("job_group")
    private String jobGroup;

    @ApiModelProperty(value = "创建者")
    @TableField("create_user")
    private String createUser;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(value = "更新者")
    @TableField("update_user")
    private String updateUser;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_time")
    private Date updateTime;

}