package withouther.system.plugins.monitor.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 系统日志
 *
 * @author tzq
 * @date 2020-04-09
 */
@Data
@TableName("sys_log")
@ApiModel(value = "Log对象")
public class Log implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    @TableField("id")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "描述")
    @TableField("description")
    private String description;

    @ApiModelProperty(value = "异常信息")
    @TableField("exception_detail")
    private String exceptionDetail;

    @ApiModelProperty(value = "日志类型", example = "1")
    @TableField("log_type")
    private Integer logType;

    @ApiModelProperty(value = "参数")
    @TableField("params")
    private String params;

    @ApiModelProperty(value = "来源ip")
    @TableField("request_ip")
    private String requestIp;

    @ApiModelProperty(value = "用户名")
    @TableField("username")
    private String username;

    @ApiModelProperty(value = "请求地址")
    @TableField("address")
    private String address;

    @ApiModelProperty(value = "浏览器")
    @TableField("browser")
    private String browser;

    @ApiModelProperty(value = "请求耗时")
    @TableField("time")
    private Long time;

}