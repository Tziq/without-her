package withouther.system.plugins.system.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;


import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("sys_user")
public class User implements Serializable  {

    @TableId(value = "id", type = IdType.AUTO)
    @TableField("id")
	private Long id;

    @TableField(value="org_id")
	private Long orgId; //组织机构id

    @TableField(value="org_name")
	private String orgName; //组织机构名称

    @TableField(value="user_name")
	private String userName;//帐号

    @TableField(value="nick_name")
    private String nickName;//名称（昵称或者真实姓名，不同系统不同定义）

    @TableField(value="pass_word")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password; //密码;

    @TableField(value="status")
    private Integer status;//用户状态,0:创建未认证（比如没有激活，没有输入验证码等等）--等待验证的用户 , 1:正常状态,2：用户被锁定.

    @TableField(value="create_time")
    private Date createTime;

    @TableField(value="sex")
    private String sex;

    @TableField(value="phone")
    private String phone;

    @TableField(value="email")
    private String email;

    @ApiModelProperty(hidden = true)
    @TableField(exist = false)
    private Set<Role> roleList;// 一个用户具有多个角色

    @ApiModelProperty(hidden = true)
    @TableField(exist = false)
    private List<Menu> menus;// 一个用户具有多个权限

}
