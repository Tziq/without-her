package withouther.system.plugins.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import withouther.system.core.base.BaseServiceImpl;
import withouther.system.core.util.page.PageQuery;
import withouther.system.core.util.page.PageUtils;

import withouther.system.plugins.system.entity.RoleMenu;
import withouther.system.plugins.system.entity.UserRole;
import withouther.system.plugins.system.mapper.RoleMenuMapper;
import withouther.system.plugins.system.service.RoleMenuService;


@Service("RoleMenuService")
public class RoleMenuServiceImpl extends BaseServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<RoleMenu> page = this.page(
                new PageQuery<RoleMenu>().getPage(params),
                new QueryWrapper<RoleMenu>()
        );
        return new PageUtils(page);
    }

    @Override
    public List<RoleMenu> getRoleMenus(Long id) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("rid", id);
        return this.list(queryWrapper);
    }

    @Override
    public boolean addRoleMenus(Set<RoleMenu> roleMenus) {
        return this.baseMapper.insertRoleMenus(roleMenus) > 0;
    }

    @Override
    public boolean deleteRoleMenus(Long rid) {
        UpdateWrapper<RoleMenu > wrapper = new UpdateWrapper<>();
        wrapper.eq("rid", rid);
        return  this.remove(wrapper);
    }

}