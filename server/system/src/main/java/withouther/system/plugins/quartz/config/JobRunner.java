package withouther.system.plugins.quartz.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @ClassName JobRunner
 * @Auther: tzq
 * @Date: 2021/2/20 10:59
 * @Description:
 */
@Component
@Slf4j
public class JobRunner implements ApplicationRunner {

    //注入定时任务管理器
    @Resource
    private JobQuartzManager quartzManager;

    /**
     * 项目启动时激活定时任务
     */
    @Override
    public void run(ApplicationArguments applicationArguments) {
        log.info("--------------------注入定时任务---------------------");
        quartzManager.start();
        log.info("--------------------定时任务注入完成---------------------");
    }

}
