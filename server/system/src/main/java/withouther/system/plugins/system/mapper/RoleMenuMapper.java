package withouther.system.plugins.system.mapper;

import withouther.system.plugins.system.entity.RoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import withouther.system.plugins.system.entity.UserRole;

import java.util.Set;

/**
 * ${comments}
 * 
 * @author tzq
 * @email ${email}
 * @date ${datetime}
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
    int insertRoleMenus(Set<RoleMenu> roles);
}
