package withouther.system.plugins.system.service;

import withouther.system.core.base.BaseService;
import withouther.system.core.util.page.PageUtils;
import withouther.system.plugins.system.entity.Role;

import java.util.List;
import java.util.Map;

/**
 * 角色管理 服务类
 *
 * @author tzq
 * @date 2020-03-20
 */
public interface RoleService extends BaseService<Role> {

    PageUtils queryPage(Map<String, Object> params);

    List<Role> selectUserRoleListByUserId(Long userId);

    Map<String, Object> getRoleMenus(Long id);

    boolean addRole(Role role);

    boolean updateRole(Role role);

    boolean deleteRoles(List<Long> ids);
}

