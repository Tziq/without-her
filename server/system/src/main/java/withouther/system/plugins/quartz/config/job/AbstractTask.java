package withouther.system.plugins.quartz.config.job;

import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

/**
 * @ClassName AbstractTask
 * @Auther: tzq
 * @Date: 2021/2/20 10:55
 * @Description:
 * 定时任务抽象类
 */
@Slf4j
public abstract class AbstractTask implements Job {

    protected abstract void executeInternal(JobExecutionContext context) throws Exception;

    /**
     * 定时任务标识
     */
    private String key;

    /**
     * 数据库里配置的主键id
     */
    private Long dataBaseId;

    @Override
    public void execute(JobExecutionContext context) {
        try {
            executeInternal(context);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            log.error("job execute failed!");
        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getDataBaseId() {
        return dataBaseId;
    }

    public void setDataBaseId(Long dataBaseId) {
        this.dataBaseId = dataBaseId;
    }
}
