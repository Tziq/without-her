package withouther.system.plugins.monitor.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import withouther.system.core.base.BaseController;
import withouther.system.core.constant.LogType;
import withouther.system.core.result.WhResult;
import withouther.system.plugins.monitor.entity.Log;
import withouther.system.plugins.monitor.service.LogService;
import withouther.system.plugins.system.entity.UserRole;

import java.util.List;
import java.util.Map;

/**
 * @author tzq
 * @date 2020-04-09
 */
@Api(tags = "系统监控：错误日志" )
@RestController
@RequestMapping("/api/logs/error" )
@Slf4j
public class ErrorLogController  extends BaseController {

    @Autowired
    private LogService logService;
    @ApiOperation("列表" )
    @GetMapping("/list" )
    public WhResult page(@RequestParam Map<String, Object> params) {
        try {

            params.put("logType", 0);
            return WhResult.success(logService.queryPage(params));
        } catch (Exception e) {
            log.error("page error",e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @ApiOperation("删除全部" )
    @DeleteMapping("/deleteAll" )
    public WhResult delete() {
        try {
            UpdateWrapper<Log> wrapper = new UpdateWrapper<>();
            wrapper.eq("log_type", LogType.ERROR);
            return  WhResult.success(logService.remove(wrapper));
        } catch (Exception e) {
            log.error("delete error",e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }
}
