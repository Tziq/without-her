package withouther.system.plugins.system.entity.dto;

import lombok.Data;
import withouther.system.plugins.system.entity.Menu;

import java.io.Serializable;
import java.util.List;

@Data
public class MenuDto implements Serializable {

    private Long id;

    private String name;

    private String resourceType;

    private String url;

    private String permission;

    private Long parentId;

    private String parentName;

    private String icon;

    private Integer available;

    private List<MenuDto> children;
}
