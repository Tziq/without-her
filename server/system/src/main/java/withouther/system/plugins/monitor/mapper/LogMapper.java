package withouther.system.plugins.monitor.mapper;

import withouther.system.plugins.monitor.entity.Log;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * ${comments}
 * 
 * @author tzq
 * @email ${email}
 * @date ${datetime}
 */
public interface LogMapper extends BaseMapper<Log> {
	
}
