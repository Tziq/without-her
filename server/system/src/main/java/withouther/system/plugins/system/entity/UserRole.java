package withouther.system.plugins.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.*;

/**
 * @author tzq
 * @date 2020-03-20
 */
@Data
@TableName("sys_user_role")
@ApiModel(value = "UserRole对象")
public class UserRole implements Serializable {

    @ApiModelProperty(value = "用户id", example = "1")
    @TableField("uid")
    private Long uid;

    @ApiModelProperty(value = "角色id", example = "1")
    @TableField("rid")
    private Long rid;

}