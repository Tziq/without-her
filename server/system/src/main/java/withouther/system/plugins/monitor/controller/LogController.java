package withouther.system.plugins.monitor.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;
import java.util.List;

import withouther.system.core.aop.log.Log;
import withouther.system.core.constant.LogType;
import withouther.system.plugins.monitor.service.LogService;
import withouther.system.core.base.BaseController;
import withouther.system.core.result.WhResult;
import withouther.system.plugins.system.entity.Role;
import withouther.system.plugins.system.entity.UserRole;


/**
* @author tzq
* @date 2020-04-09
*/
@Api(tags = "系统监控：日志" )
@RestController
@RequestMapping("/api/logs" )
@Slf4j
public class LogController extends BaseController {

    @Autowired
    private LogService  logService;


    @ApiOperation("列表" )
    @GetMapping("/list" )
    public WhResult page(@RequestParam Map<String, Object> params) {
        return WhResult.success(logService.queryPage(params));
    }

    @Log("删除日志")
    @ApiOperation("删除全部" )
    @DeleteMapping("/deleteAll" )
    public WhResult delete() {
        UpdateWrapper<Role> wrapper = new UpdateWrapper<>();
        wrapper.eq("log_type", LogType.INFO);
        return  WhResult.success(logService.removeById(wrapper));
    }
}