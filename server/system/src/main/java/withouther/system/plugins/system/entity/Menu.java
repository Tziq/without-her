package withouther.system.plugins.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.*;
import withouther.system.plugins.system.entity.dto.MenuDto;

/**
 * @author tzq
 * @date 2020-04-09
 */
@Data
@TableName("sys_menu")
@ApiModel(value = "Menu对象")
public class Menu implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    @TableField("id")
    private Long id;

    @ApiModelProperty(value = "权限名称")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "类型  [menu|button]")
    @TableField("resource_type")
    private String resourceType;

    @ApiModelProperty(value = "0按钮；1一级菜单 2 二级菜单  url页面")
    @TableField("url")
    private String url;

    @ApiModelProperty(value = "权限码 menu例子：role:*，button例子：role:create,role:update,role:delete,role:view")
    @TableField("permission")
    private String permission;

    @ApiModelProperty(value = "父id", example = "1")
    @TableField("parent_id")
    private Long parentId;

    @ApiModelProperty(value = "图标，系统配置")
    @TableField("icon")
    private String icon;

    @ApiModelProperty(value = "0 不可用 1可用", example = "1")
    @TableField("available")
    private Integer available;

    @ApiModelProperty(value = "组件名称")
    @TableField("component_name")
    private String componentName;

    @ApiModelProperty(value = "组件路径")
    @TableField("component")
    private String component;

    @TableField(exist = false)
    private List<Menu> children;

    @TableField(exist = false)
    private List<Role> roles;

}