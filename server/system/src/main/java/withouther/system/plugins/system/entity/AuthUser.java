package withouther.system.plugins.system.entity;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @ClassName AuthUser
 * @Auther: tzq
 * @Date: 2020/3/25 15:03
 * @Description:
 */
@Data
public class AuthUser {
    @NotBlank( message = "用户名不能为空")
    private String username;

    @NotBlank(message = "密码不能为空")
    private String password;

    @NotBlank(message = "图形验证码不能为空")
    private String code;

    private String uuid;
}
