package withouther.system.plugins.monitor.service;

import org.aspectj.lang.JoinPoint;
import withouther.system.core.base.BaseService;
import withouther.system.core.util.page.PageUtils;
import withouther.system.plugins.monitor.entity.Log;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 系统日志 服务类
 *
 * @author tzq
 * @date 2020-04-09
 */
public interface LogService extends BaseService<Log> {
    PageUtils queryPage(Map<String, Object> params);

    void save(HttpServletRequest request, JoinPoint join, Log log);
}

