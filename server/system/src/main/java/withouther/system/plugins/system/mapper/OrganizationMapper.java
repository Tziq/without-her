package withouther.system.plugins.system.mapper;

import withouther.system.plugins.system.entity.Organization;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * ${comments}
 * 
 * @author tzq
 * @email ${email}
 * @date ${datetime}
 */
public interface OrganizationMapper extends BaseMapper<Organization> {
	
}
