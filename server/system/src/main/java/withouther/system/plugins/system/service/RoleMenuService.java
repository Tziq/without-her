package withouther.system.plugins.system.service;

import withouther.system.core.base.BaseService;
import withouther.system.core.util.page.PageUtils;
import withouther.system.plugins.system.entity.RoleMenu;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *  服务类
 *
 * @author tzq
 * @date 2020-04-09
 */
public interface RoleMenuService extends BaseService<RoleMenu> {
    PageUtils queryPage(Map<String, Object> params);

    List<RoleMenu> getRoleMenus(Long id);

    boolean addRoleMenus(Set<RoleMenu> roleMenus);

    boolean deleteRoleMenus(Long rid);
}

