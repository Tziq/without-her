package withouther.system.plugins.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.*;

/**
 * @author tzq
 * @date 2020-03-20
 */
@Data
@TableName("sys_organization")
@ApiModel(value = "Organization对象")
public class Organization implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键,新增不传", example = "1")
    @TableField("id")
    private Long id;

    @ApiModelProperty(value = "机构名")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "机构编号 查重")
    @TableField("code")
    private String code;

    @ApiModelProperty(value = "上级id", example = "1")
    @TableField("parent_id")
    private Long parentId;

    @ApiModelProperty(value = "上级name")
    @TableField("parent_name")
    private String parentName;

    @ApiModelProperty(value = "排序用")
    @TableField("index_code")
    private String indexCode;

    @ApiModelProperty(value = "备注说明")
    @TableField("comment")
    private String comment;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private Date createTime;

}