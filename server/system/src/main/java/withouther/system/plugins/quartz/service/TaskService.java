package withouther.system.plugins.quartz.service;

import org.quartz.SchedulerException;
import withouther.system.core.base.BaseService;
import withouther.system.core.util.page.PageUtils;
import withouther.system.plugins.quartz.entity.Task;

import java.util.Map;

/**
 *  服务类
 *
 * @author tzq
 * @date 2021-02-19
 */
public interface TaskService extends BaseService<Task> {
    PageUtils queryPage(Map<String, Object> params);

    void initSchedule() throws SchedulerException;
}

