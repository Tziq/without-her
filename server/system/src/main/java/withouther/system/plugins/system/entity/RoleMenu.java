package withouther.system.plugins.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 
 * @author tzq
 * @date 2020-04-09
 */
@Data
@TableName("sys_role_menu")
@ApiModel(value="RoleMenu对象")
public class RoleMenu implements Serializable {

@TableField("rid")
private Long rid;

@TableField("mid")
private Long mid;

}