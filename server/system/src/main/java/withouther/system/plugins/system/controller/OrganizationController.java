package withouther.system.plugins.system.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.Map;
import java.util.List;

import withouther.system.core.aop.log.Log;
import withouther.system.plugins.system.entity.Organization;
import withouther.system.plugins.system.service.OrganizationService;
import withouther.system.core.base.BaseController;
import withouther.system.core.result.WhResult;



/**
* @author tzq
* @date 2020-03-20
*/
@Api(tags = "系统：机构管理" )
@RestController
@RequestMapping("/api/org" )
@Slf4j
public class OrganizationController extends BaseController {

    @Autowired
    private OrganizationService  organizationService;

    @Log("查询机构")
    @ApiOperation("查询机构" )
    @GetMapping("/list" )
    @PreAuthorize("hasAuthority('system:org:view')")
    public WhResult page(@RequestParam Map<String, Object> params) {
        try {
            return WhResult.success(organizationService.buildTree(params));
        } catch (Exception e) {
            log.error("page error",e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @Log("获取机构详情")
    @ApiOperation("获取详情" )
    @GetMapping("/get/{id}" )
    @PreAuthorize("hasAuthority('system:org:view')")
    public WhResult get(@PathVariable("id") Long id) {
        try {
            return WhResult.success(organizationService.getById(id));
        } catch (Exception e) {
            log.error("get error",e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @Log("添加机构")
    @ApiOperation("添加" )
    @PostMapping("/add" )
    @PreAuthorize("hasAuthority('system:org:add')")
    public WhResult add(@RequestBody Organization organization) {
        try {
            return WhResult.success(organizationService.save( organization ));
        } catch (Exception e) {
            log.error("add error",e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @Log("修改机构")
    @ApiOperation("修改" )
    @PutMapping("/update" )
    @PreAuthorize("hasAuthority('system:org:upd')")
    public WhResult update(@RequestBody Organization organization) {
        try {
            return WhResult.success(organizationService.updateById( organization ));
        } catch (Exception e) {
            log.error("modify error",e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @Log("删除机构")
    @ApiOperation("删除" )
    @DeleteMapping("/delete" )
    @PreAuthorize("hasAuthority('system:org:del')")
    public WhResult delete(@RequestBody List<Long> ids) {
        try {
            return  WhResult.success(organizationService.removeByIds(ids));
        } catch (Exception e) {
            log.error("delete error",e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }
}