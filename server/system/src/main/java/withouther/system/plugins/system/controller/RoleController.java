package withouther.system.plugins.system.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.List;

import withouther.system.core.aop.log.Log;
import withouther.system.plugins.system.entity.Role;
import withouther.system.plugins.system.service.RoleService;
import withouther.system.core.base.BaseController;
import withouther.system.core.result.WhResult;

import javax.validation.Valid;


/**
 * @author tzq
 * @date 2020-03-20
 */
@Api(tags = "系统：角色管理")
@RestController
@RequestMapping("api/role")
@Slf4j
public class RoleController extends BaseController {

    @Autowired
    private RoleService roleService;

    @Log("查询角色")
    @ApiOperation("分页")
    @GetMapping("/list")
    @PreAuthorize("hasAuthority('system:role:view')")
    public WhResult page(@RequestParam Map<String, Object> params) {
        try {
//            System.out.println(roleService.queryPage(params));
            return WhResult.success(roleService.queryPage(params));
        } catch (Exception e) {
            log.error("page error", e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @Log("获取角色详情")
    @ApiOperation("获取详情")
    @GetMapping("/get/{id}")
    @PreAuthorize("hasAuthority('system:role:view')")
    public WhResult get(@PathVariable("id") Long id) {
        try {
            return WhResult.success(roleService.getById(id));
        } catch (Exception e) {
            log.error("get error", e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @Log("添加角色")
    @ApiOperation("添加")
    @PostMapping("/add")
    @PreAuthorize("hasAuthority('system:role:add')")
    public WhResult add(@Valid @RequestBody Role role) {
        try {
            return WhResult.success(roleService.addRole(role));
        } catch (Exception e) {
            log.error("add error", e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @Log("修改角色")
    @ApiOperation("修改")
    @PostMapping("/update")
    @PreAuthorize("hasAuthority('system:role:upd')")
    public WhResult modify(@RequestBody Role role) {
        try {
            return WhResult.success(roleService.updateRole(role));
        } catch (Exception e) {
            log.error("modify error", e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @Log("删除角色")
    @ApiOperation("删除")
    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('system:role:del')")
    public WhResult delete(@RequestBody List<Long> ids) {
        try {
            return WhResult.success(roleService.deleteRoles(ids));
        } catch (Exception e) {
            log.error("delete error", e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @Log("获取角色绑定的菜单")
    @ApiOperation("获取角色绑定的菜单")
    @GetMapping("/getRoleMenus/{id}")
    @PreAuthorize("hasAuthority('system:role:view')")
    public WhResult getRoleMenus(@PathVariable("id") Long id) {
        try {
            return WhResult.success(roleService.getRoleMenus(id));
        } catch (Exception e) {
            log.error("delete error", e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }
}