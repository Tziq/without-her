package withouther.system.plugins.system.entity.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
public class OrganizationDto {
    private Long id;

    private String name;

    @NotNull
    private Boolean enabled;

    private Long parentId;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<OrganizationDto> children;

    private Date createTime;

    public String getLabel() {
        return name;
    }
}
