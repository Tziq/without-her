package withouther.system.plugins.quartz.mapper;

import withouther.system.plugins.quartz.entity.Task;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * ${comments}
 * 
 * @author tzq
 * @email ${email}
 * @date ${datetime}
 */
public interface TaskMapper extends BaseMapper<Task> {
	
}
