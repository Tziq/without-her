package withouther.system.plugins.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import withouther.system.plugins.system.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import withouther.system.plugins.system.entity.UserRole;

import java.util.List;

/**
 * ${comments}
 * 
 * @author tzq
 * @email ${email}
 * @date ${datetime}
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

    List<Role> selectUserRoleListByUserId(Long userId);
}
