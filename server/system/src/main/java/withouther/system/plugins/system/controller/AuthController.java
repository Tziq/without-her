package withouther.system.plugins.system.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.schema.Maps;
import withouther.system.core.base.BaseController;
import withouther.system.core.result.WhResult;
import withouther.system.core.security.entity.UserInfo;
import withouther.system.core.util.RedisCacheManager;
import withouther.system.core.util.SecurityUtil;
import withouther.system.plugins.system.entity.AuthUser;
import withouther.system.plugins.system.entity.Menu;
import withouther.system.plugins.system.entity.User;
import withouther.system.plugins.system.entity.dto.RouterVo;
import withouther.system.plugins.system.service.MenuService;
import withouther.system.plugins.system.service.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/auth" )
@Slf4j
public class AuthController extends BaseController {


    @Resource
    private UserService userService;

    @Resource
    private RedisCacheManager redisCacheManager;

    @Resource
    private MenuService menuService;

    /**
     * 登录
     *
     * @param authUser
     * @return
     */
    @PostMapping(value = "/login")
    @ApiOperation("登录" )
    public WhResult login(@RequestBody AuthUser authUser, HttpServletRequest request) {
//        // 社交快速登录
//        String token = request.getParameter("token");
//        if (StrUtil.isNotEmpty(token)) {
//            return WhResult.success(token);
//        }
        return WhResult.success(userService.login(authUser));
    }

    @ApiOperation("获取用户信息")
    @GetMapping(value = "/info")
    public WhResult getUserInfo(){
        return WhResult.success(userService.getUserInfo());
    }


    @ApiOperation("获取验证码")
    @GetMapping(value = "/code")
    public WhResult getCode() {
        //定义图形验证码的长、宽、验证码字符数、干扰元素个数
        try {
            CircleCaptcha captcha = CaptchaUtil.createCircleCaptcha(111, 36,4,0);
            String code = captcha.getCode();
            String uuid = IdUtil.simpleUUID();
            log.info("code = " + code);
            // 保存
            redisCacheManager.set(uuid, code, 60);
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("uuid", uuid);
            resultMap.put("img", captcha.getImageBase64());
            return WhResult.success(resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            return WhResult.fail();
        }
    }

    /**
     * 退出登录
     *
     * @return 路由信息
     */
    @ApiOperation("路由")
    @DeleteMapping(value = "/logout")
    public WhResult logout(HttpServletRequest request)
    {
        return WhResult.success();
    }
}
