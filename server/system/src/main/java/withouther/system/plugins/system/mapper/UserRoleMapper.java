package withouther.system.plugins.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import withouther.system.plugins.system.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Set;

/**
 * ${comments}
 *
 * @author tzq
 * @email ${email}
 * @date ${datetime}
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {
    int inserts(Set<UserRole> roles);
}
