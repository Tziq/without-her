package withouther.system.plugins.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.*;

/**
 * @author tzq
 * @date 2020-03-20
 */
@Data
@TableName("sys_role_permission")
@ApiModel(value = "RolePermission对象")
public class RolePermission implements Serializable {

    @TableField("rid")
    private Long rid;

    @TableField("pid")
    private Long pid;

}