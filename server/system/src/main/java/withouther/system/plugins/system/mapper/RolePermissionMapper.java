package withouther.system.plugins.system.mapper;

import withouther.system.plugins.system.entity.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * ${comments}
 * 
 * @author tzq
 * @email ${email}
 * @date ${datetime}
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {
	
}
