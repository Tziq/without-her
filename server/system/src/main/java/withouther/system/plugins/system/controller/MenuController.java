package withouther.system.plugins.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.sql.Wrapper;
import java.util.Map;
import java.util.List;

import withouther.system.core.aop.log.Log;
import withouther.system.core.security.entity.UserInfo;
import withouther.system.core.util.SecurityUtil;
import withouther.system.plugins.system.entity.Menu;
import withouther.system.plugins.system.entity.dto.MenuDto;
import withouther.system.plugins.system.entity.dto.RouterVo;
import withouther.system.plugins.system.service.MenuService;
import withouther.system.core.base.BaseController;
import withouther.system.core.result.WhResult;



/**
* @author tzq
* @date 2020-04-09
*/
@Api(tags = "菜单管理" )
@RestController
@RequestMapping("api/menus" )
@Slf4j
public class MenuController extends BaseController {

    @Autowired
    private MenuService  menuService;

    @Log("查询菜单")
    @ApiOperation("分页" )
    @GetMapping("/list" )
    @PreAuthorize("hasAuthority('system:menu:view')")
    public WhResult Object(@RequestParam Map<String, Object> params) {
        try {

            List<Menu> menuDtoList = menuService.listMenu(params);
            log.info(menuDtoList.toString());
            return WhResult.success(menuService.buildTree(menuDtoList));
        } catch (Exception e) {
            log.error("page error",e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @ApiOperation("返回全部的菜单")
    @GetMapping(value = "/tree")
    public WhResult getMenuTree(){
            return WhResult.success(menuService.getMenuTree(menuService.findByParentId(0L)));
    }

    @Log("查询菜单详情")
    @ApiOperation("获取详情" )
    @GetMapping("/get/{id}" )
    @PreAuthorize("hasAuthority('system:menu:view')")
    public WhResult get(@PathVariable("id") Long id) {
        try {
            return WhResult.success(menuService.getById(id));
        } catch (Exception e) {
            log.error("get error",e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @Log("添加菜单")
    @ApiOperation("添加" )
    @PostMapping("/add" )
    @PreAuthorize("hasAuthority('system:menu:add')")
    public WhResult add(@RequestBody Menu menu) {
        try {
            return WhResult.success(menuService.addMenu( menu ));
        } catch (Exception e) {
            log.error("add error",e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @Log("修改菜单")
    @ApiOperation("修改" )
    @PutMapping("/update" )
    @PreAuthorize("hasAuthority('system:menu:upd')")
    public WhResult update(@RequestBody Menu menu) {
        try {
            return WhResult.success(menuService.updateById( menu ));
        } catch (Exception e) {
            log.error("modify error",e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @Log("删除菜单")
    @ApiOperation("删除" )
    @DeleteMapping("/delete" )
    @PreAuthorize("hasAuthority('system:menu:del')")
    public WhResult delete(@RequestBody List<Long> ids) {
        try {
            return  WhResult.success(menuService.removeByIds(ids));
        } catch (Exception e) {
            log.error("delete error",e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }

    @ApiOperation("获取前端菜单" )
    @GetMapping("/build" )
    public WhResult build() {
        try {
            UserInfo userInfo = SecurityUtil.getUser();
            List<Menu> menus = menuService.findPermsByUserId(userInfo.getId());
            List<RouterVo> routerVos = menuService.buildMenus(menus);
            return WhResult.success(routerVos);
        } catch (Exception e) {
            log.error("delete error",e);
            return WhResult.fail("系统繁忙，请稍后再试");
        }
    }
}