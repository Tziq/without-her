package withouther.system.plugins.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @author tzq
 * @since 2020-03-20
 */
@Data
@TableName("sys_role")
@ApiModel(value = "Role对象")
public class Role implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    @TableField("id")
    private Long id;

    @ApiModelProperty(value = "角色名")
    @TableField("name")
    @NotNull(message = "角色名称不能为空")
    private String name;

    @ApiModelProperty(value = "所属组织机构", example = "1")
    @TableField("org_id")
    private Long orgId;

    @ApiModelProperty(value = "描述")
    @TableField("description")
    @NotNull(message = "描述不能为空")
    private String description;

    @ApiModelProperty(value = "0 fase 1 true")
    @TableField("available")
    private Integer available;

    @ApiModelProperty(value = "级别")
    @TableField("level")
    private Integer level;

    //角色 -- 权限关系：多对多关系;
    @ApiModelProperty(hidden = true)
    @TableField(exist = false)
    private List<Menu> menus;
    //用户 -- 角色关系定义;
    @ApiModelProperty(hidden = true)
    @TableField(exist = false)
    private List<User> userInfos;// 一个角色对应多个用户
}