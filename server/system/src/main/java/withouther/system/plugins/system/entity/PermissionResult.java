package withouther.system.plugins.system.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import java.io.Serializable;
import java.util.List;

/**
 * 返回给前端时使用
 * @author tzq
 * @date 2020-03-20
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PermissionResult implements Serializable {

    private String name;

    private String path;

    private Boolean hidden;

    private String redirect;

    private String component;

    private Boolean alwaysShow;

    private Meta meta;

    private List<PermissionResult> children;

    public void setMeta(String title, String icon, Boolean noCache) {
        this.meta = new Meta(title, icon, noCache);
    }
    class Meta {
        private String title;

        private String icon;

        private Boolean noCache;

        public Meta() {}

        public Meta(String title, String icon, Boolean noCache) {
            this.title = title;
            this.icon = icon;
            this.noCache = noCache;
        }
    }
}
