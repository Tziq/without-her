package withouther.system.plugins.quartz.config.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import withouther.system.plugins.quartz.service.TaskService;

/**
 * @ClassName ScheduleJobInitListener
 * @Auther: tzq
 * @Date: 2021/2/19 15:17
 * @Description:
 */
@Component
@Order(value = 1)
public class ScheduleJobInitListener implements CommandLineRunner {

    @Autowired
    TaskService scheduleJobService;

    @Override
    public void run(String... arg0) throws Exception {
        try {
            scheduleJobService.initSchedule();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
