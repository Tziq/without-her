package withouther.system.plugins.system.mapper;

import withouther.system.plugins.system.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * ${comments}
 * 
 * @author tzq
 * @email ${email}
 * @date ${datetime}
 */
public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> findPermsByUserId(Long userId);

    List<Menu> selectRoleMenusByRoleId(Long roleId);
}
