package withouther.system.plugins.quartz.config.job;

import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;

/**
 * @ClassName JobTestTask
 * @Auther: tzq
 * @Date: 2021/2/20 10:56
 * @Description:
 */
@Component("JobTestTask")
public class JobTestTask extends AbstractTask {

    @Override
    protected void executeInternal(JobExecutionContext context) {
        System.out.println("key = " + this.getKey());
        System.out.println("dataBaseId = " + this.getDataBaseId());
    }

}