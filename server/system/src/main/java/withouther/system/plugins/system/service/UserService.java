package withouther.system.plugins.system.service;


import withouther.system.core.base.BaseService;
import withouther.system.core.security.entity.UserInfo;
import withouther.system.core.util.page.PageUtils;
import withouther.system.plugins.system.entity.AuthUser;
import withouther.system.plugins.system.entity.User;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface UserService extends BaseService<User> {

	PageUtils queryPage(Map<String, Object> params);

	/**
	 * 添加用户
	 * @param user
	 * @return
	 */
	boolean addUser(User user);

	/**
	 * 修改用户
	 * @param user
	 * @return
	 */
	boolean updateUser(User user);

	/**
	 * 删除用户表并且删除用户角色关联表数据
	 * @param ids
	 * @return
	 */
	boolean deleteUserByIdAndRole(List<Long> ids);

	Map<String, Object> login(AuthUser authUser);

	Set<String> findPermsByUserId(Long userId);

	Set<String> findRoleIdByUserId(Long userId);

	UserInfo getUserInfo();
}