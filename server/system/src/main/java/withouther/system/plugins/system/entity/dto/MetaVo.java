package withouther.system.plugins.system.entity.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MetaVo {
    /**
     * 设置该路由在侧边栏和面包屑中展示的名字
     */
    private String title;

    /**
     * 设置该路由的图标，对应路径src/icons/svg
     */
    private String icon;
}
