package withouther.system.plugins.system.service;

import withouther.system.core.base.BaseService;
import withouther.system.core.util.page.PageUtils;
import withouther.system.plugins.system.entity.Organization;
import withouther.system.plugins.system.entity.dto.OrganizationDto;

import java.util.List;
import java.util.Map;

/**
 *  服务类
 *
 * @author tzq
 * @date 2020-03-20
 */
public interface OrganizationService extends BaseService<Organization> {
    PageUtils queryPage(Map<String, Object> params);

    List<OrganizationDto> listOrg(Map<String, Object> params);

    Object buildTree(Map<String, Object> params);

    boolean removeByIds(List<Long> ids);

    boolean updateById(Organization organization);
}

