package withouther.system.plugins.quartz.service.impl;

import org.quartz.SchedulerException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import withouther.system.core.base.BaseServiceImpl;
import withouther.system.core.util.page.PageQuery;
import withouther.system.core.util.page.PageUtils;

import withouther.system.plugins.quartz.entity.Task;
import withouther.system.plugins.quartz.mapper.TaskMapper;
import withouther.system.plugins.quartz.service.TaskService;


@Service("TaskService")
public class TaskServiceImpl extends BaseServiceImpl<TaskMapper, Task> implements TaskService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Task> page = this.page(
                new PageQuery<Task>().getPage(params),
                new QueryWrapper<Task>()
        );
        return new PageUtils(page);
    }

    @Override
    public void initSchedule() throws SchedulerException {
        // 这里获取任务信息数据
        List<Task> jobList = this.list();
        for (Task task : jobList) {
//            if (JobStatusEnum.RUNNING.getCode().equals(task.getJobStatus())) {
//                quartzManager.addJob(task);
//            }
        }
    }

}