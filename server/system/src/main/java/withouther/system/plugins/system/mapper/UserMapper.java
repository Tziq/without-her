package withouther.system.plugins.system.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import withouther.system.plugins.system.entity.User;

@Mapper
public interface UserMapper extends BaseMapper<User> {

	IPage<User> selectUserListByParam(Page<User> ipage, @Param("map")Map<String, Object> param);

	int countUser(@Param("map")Map<String, Object> param);
}