package withouther.system.plugins.system.service;

import withouther.system.core.base.BaseService;
import withouther.system.core.util.page.PageUtils;
import withouther.system.plugins.system.entity.Menu;
import withouther.system.plugins.system.entity.dto.RouterVo;

import java.util.List;
import java.util.Map;

/**
 *  服务类
 *
 * @author tzq
 * @date 2020-04-09
 */
public interface MenuService extends BaseService<Menu> {
    PageUtils queryPage(Map<String, Object> params);

    Menu addMenu(Menu menu);
    /**
     * 获取菜单树
     * @param menus /
     * @return /
     */
    Object getMenuTree(List<Menu> menus);
    /**
     * 根据pid查询
     * @param parentId /
     * @return /
     */
    List<Menu> findByParentId(long parentId);
    /**
     * 构建菜单树
     * @param menuDtos 原始数据
     * @return /
     */
    Map<String,Object> buildTree(List<Menu> menuDtos);

    /**
     * 查询菜单
     * @param paramMap
     * @return
     */
    List<Menu > listMenu(Map<String, Object> paramMap);

    boolean updateMenu(Menu menu);

    boolean removeByIds(List<Long> ids);

    List<Menu> findPermsByUserId(Long userId);

    List<Menu> selectRoleMenusByRoleId(Long userId);

    List<RouterVo> buildMenus(List<Menu> menus);

}

