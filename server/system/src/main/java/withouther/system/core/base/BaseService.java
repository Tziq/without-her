package withouther.system.core.base;

import java.io.Serializable;

import com.baomidou.mybatisplus.extension.service.IService;

public interface BaseService<E extends Serializable> extends IService<E> {
	
}
