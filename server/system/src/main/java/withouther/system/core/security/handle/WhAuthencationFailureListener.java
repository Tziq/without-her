package withouther.system.core.security.handle;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.*;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * @Classname WhAuthencationFailureListener
 * @Description 用户登录失败监听器事件
 * @Author tzq
 * @Date 2020/03/19 3:35 下午
 * @Version 1.0
 */
@Component
@Slf4j
public class WhAuthencationFailureListener implements ApplicationListener<AbstractAuthenticationFailureEvent> {

    @Override
    public void onApplicationEvent(AbstractAuthenticationFailureEvent event) {
        log.info("onApplicationEvent监听器");
//        UsernameNotFoundException
//        throw new PreBaseException("用户名或者密码错误", 402);
            if (event instanceof AuthenticationFailureBadCredentialsEvent) {
            //提供的凭据是错误的，用户名或者密码错误
//            throw new PreBaseException("用户名或者密码错误", 402);
        } else if (event instanceof AuthenticationFailureCredentialsExpiredEvent) {
            //验证通过，但是密码过期
//            throw new PreBaseException("密码过期", 402);
        } else if (event instanceof AuthenticationFailureDisabledEvent) {
            //验证过了但是账户被禁用
//            throw new PreBaseException("账户被禁用", 402);
        } else if (event instanceof AuthenticationFailureExpiredEvent) {
            //验证通过了，但是账号已经过期
//            throw new PreBaseException("账号已经过期", 402);
        } else if (event instanceof AuthenticationFailureLockedEvent) {
            //账户被锁定
//            throw new PreBaseException("账户被锁定", 402);
        } else if (event instanceof AuthenticationFailureProviderNotFoundEvent) {
            //配置错误，没有合适的AuthenticationProvider来处理登录验证
//            throw new PreBaseException("配置错误", 402);
        } else if (event instanceof AuthenticationFailureProxyUntrustedEvent) {
            //代理不受信任，用于Oauth、CAS这类三方验证的情形，多属于配置错误
//            throw new PreBaseException("代理不受信任", 402);
        } else if (event instanceof AuthenticationFailureServiceExceptionEvent) {
            //其他任何在AuthenticationManager中内部发生的异常都会被封装成此类
//            throw new PreBaseException("内部发生错误", 402);
        }
    }

}