package withouther.system.core.constant;

import java.io.Serializable;

public class ConstantKey implements Serializable {
    /**
     * 签名key
     */
    public static final String SIGNING_KEY = "spring-security-@Jwt!&Secret^#";
}
