package withouther.system.core.result;

import java.io.Serializable;

/**
 * 返回接口
 * @author bole
 *
 * @param <T>
 */
@SuppressWarnings("all")
public class WhResult implements Serializable{
	
	private boolean isSuccess;
	private int code;
	private String msg;
	private Object data;
	
	private enum ResponseCode{
		SUCCESS(200, "SUCCESS"), ERROR(500, "ERROR");
		private final int code;
		private final String desc;
		private ResponseCode(int code, String desc) {
			this.code = code;
		    this.desc = desc;
		}
		public int getCode(){
			
		    return this.code;
		}
		
		public String getDesc(){
			
		    return this.desc;
		}
	} 

	private WhResult(boolean isSuccess, int code){
		
		this.isSuccess = isSuccess;
		this.code = code;
	}

	private WhResult(boolean isSuccess, int code, Object data) {
		
		this.isSuccess = isSuccess;
		this.code = code;
		this.data = data;
	}

	private WhResult(boolean isSuccess, int code, String msg, Object data) {
		
		this.isSuccess = isSuccess;
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	private WhResult(boolean isSuccess, int code, String msg) {
		
		this.isSuccess = isSuccess;
		this.code = code;
		this.msg = msg;
		this.data = null;
	}

	public int getCode() {
		return this.code;
	}

	public Object getData() {
    return this.data;
	}

	public String getMsg() {
		return this.msg;
	}

	public boolean getIsSuccess() {
		return this.isSuccess;
	}

	public void setIsSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public static WhResult success() {
		return new WhResult(true, ResponseCode.SUCCESS.getCode());
	}

	public static WhResult success(String msg) {
		return new WhResult(true, ResponseCode.SUCCESS.getCode(), msg);
	}
	public static WhResult success(Object data) {
		return new WhResult(true, ResponseCode.SUCCESS.getCode(), data);
	}

	public static WhResult success(String msg, Object data) {
		return new WhResult(true, ResponseCode.SUCCESS.getCode(), msg, data);
	}
	
	public static WhResult success(int code, String msg, Object data) {
		return new WhResult(false, code, msg, data);
	}

	public static WhResult fail() {
		return new WhResult(false, ResponseCode.ERROR.getCode(), ResponseCode.ERROR.getDesc());
	}

	public static WhResult fail(String msg) {
		return new WhResult(false, ResponseCode.ERROR.getCode(), msg);
	}

	public static WhResult fail(int code, String msg) {
		return new WhResult(false, code, msg);
	}

	public static WhResult fail(int code, String msg, Object data) {
		return new WhResult(false, code, msg, data);
	}
}
