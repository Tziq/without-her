package withouther.system.core.constant;

import java.io.Serializable;

public class LogType implements Serializable {

    public static final int INFO = 1;

    public static final int ERROR = 0;
}
