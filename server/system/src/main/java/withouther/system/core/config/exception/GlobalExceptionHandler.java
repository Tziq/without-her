package withouther.system.core.config.exception;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import withouther.system.core.result.WhResult;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    private WhResult handlerErrorInfo(HttpServletRequest request, Exception e) {
        System.out.println("ArithmeticException成功拦截");
        return WhResult.success("系统繁忙，请稍后再试");
    }
}
