import request from '@/utils/request'

export function getErrDetail(id) {
  return request({
    url: 'api/logs/error/' + id,
    method: 'get'
  })
}

export function delAllError() {
  return request({
    url: 'api/logs/error/deleteAll',
    method: 'delete'
  })
}

export function delAllInfo() {
  return request({
    url: 'api/logs/deleteAll',
    method: 'delete'
  })
}
