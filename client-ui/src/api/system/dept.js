import request from '@/utils/request'

export function getDepts(params) {
  return request({
    url: 'api/org/list',
    method: 'get',
    params
  })
}

export function add(data) {
  return request({
    url: 'api/org/add',
    method: 'post',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/org/delete',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'api/org/update',
    method: 'put',
    data
  })
}

export default { add, edit, del, getDepts }
