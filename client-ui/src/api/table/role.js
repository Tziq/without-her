import request from '@/utils/request'

export function getRoles() {
    return request({
        url: 'api/role/list',
        method: 'get'
    })
}

export function getById(id) {
    return request({
        url: 'api/role/get/' + id,
        method: 'get'
    })
}

export function add(data) {
    return request({
        url: 'api/role/add',
        method: 'post',
        data: data
    })
}

export function edit(data) {
    return request({
        url: 'api/role/update',
        method: 'post',
        data: data
    })
}

export function del(data) {
    return request({
        url: 'api/role/delete',
        method: 'post',
        data: data
    })
}
export function getRoleMenus(id) {
    return request({
        url: 'api/role/getRoleMenus/' + id,
        method: 'get',
    })
}


export default { add, edit, del, getRoles, getRoleMenus }