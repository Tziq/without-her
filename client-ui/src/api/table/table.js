import request from '@/utils/request'

export function getRoles() {
  return request({
    url: 'api/role/list',
    method: 'get'
  })
}

export function getById(id) {
  return request({
    url: 'api/role/get/' + id,
    method: 'get'
  })
}

export function addRoles(data) {
  return request({
    url: 'api/role/add',
    method: 'post',
    data: data
  })
}

export function update(data) {
  return request({
    url: 'api/role/update',
    method: 'post',
    data: data
  })
}

export function delete1(data) {
  return request({
    url: 'api/role/delete',
    method: 'post',
    data: data
  })
}

